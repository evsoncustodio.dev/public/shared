#!/bin/bash

# variables for base component
baseComponent=$1
baseTecnologia=$(echo $baseComponent | cut -d- -f1)
baseSigla=$(echo $baseComponent | cut -d- -f2)
baseModulo=$(echo $baseComponent | cut -d- -f3)
baseTipo=$(echo $baseComponent | cut -d- -f4)
baseNome=$(echo $baseComponent | cut -d- -f5-10)
baseNomePascal=$(echo $baseNome | sed -r 's/(^|-)([a-z])/\U\2/g')
baseNomeCamel=$(echo $baseNome | sed -r 's/(-)([a-z])/\U\2/g')
baseNomeConst=$(echo $baseNome | sed -r 's/-/_/g' | sed -r 's/([a-z])/\U\1/g')
baseNomeDot=$(echo $baseNome | sed -r 's/-/./g')
baseNomeCapital=$(echo $baseNome | sed -r 's/(^)([a-z])/\U\2/g' | sed -r 's/(-)([a-z])/ \U\2/g')
baseNomeCram=$(echo $baseNome | sed -r 's/-//g')
baseComponentPackage=$(echo $baseComponent | sed -r 's/(sboot-|atom-|orch-)//g' | sed -r 's/-/\//g')

# variables for new component
newComponent=$2
newTecnologia=$(echo $newComponent | cut -d- -f1)
newSigla=$(echo $newComponent | cut -d- -f2)
newModulo=$(echo $newComponent | cut -d- -f3)
newTipo=$(echo $newComponent | cut -d- -f4)
newNome=$(echo $newComponent | cut -d- -f5-10)
newNomePascal=$(echo $newNome | sed -r 's/(^|-)([a-z])/\U\2/g')
newNomeCamel=$(echo $newNome | sed -r 's/(-)([a-z])/\U\2/g')
newNomeConst=$(echo $newNome | sed -r 's/-/_/g' | sed -r 's/([a-z])/\U\1/g')
newNomeDot=$(echo $newNome | sed -r 's/-/./g')
newNomeCapital=$(echo $newNome | sed -r 's/(^)([a-z])/\U\2/g' | sed -r 's/(-)([a-z])/ \U\2/g')
newNomeCram=$(echo $newNome | sed -r 's/-//g')
newComponentPackage=$(echo $newComponent | sed -r 's/(sboot-|atom-|orch-)//g' | sed -r 's/-/\//g')

echo "copy baseComponent to new newComponent ($baseComponent -> $newComponent)"
rsync -azq --exclude={'.git','data','.idea','target','*.iml'} ./$baseComponent/ ./$newComponent

# echo "rename sigla directory ($baseSigla -> $newSigla)"
# find ./$newComponent -type d -name $baseSigla -execdir rename $baseSigla $newSigla '{}' \;

echo "create base package ($newComponentPackage)"
find ./$newComponent -type d -name votorantim -exec mkdir -p {}/$newComponentPackage \;

echo "move all content to base package ($baseComponentPackage -> $newComponentPackage)"
find ./$newComponent -type d -name votorantim -exec rsync -azq --remove-source-files {}/$baseComponentPackage/ {}/$newComponentPackage \;

echo "remove empty directories"
find ./$newComponent -type d -empty -delete

echo "rename class's and directories ($baseNomePascal -> $newNomePascal)"
find ./$newComponent -iregex ".*$baseNomePascal.*" -exec rename "s/$baseNomePascal/$newNomePascal/" '{}' \;

echo "replace content in files"
echo "$baseSigla -> $newSigla"
grep -rl "$baseSigla" ./$newComponent | xargs sed -i "s/$baseSigla/$newSigla/g"
echo "$baseNome -> $newNome"
grep -rl "$baseNome" ./$newComponent | xargs sed -i "s/$baseNome/$newNome/g"
echo "$baseNomeCamel -> $newNomeCamel"
grep -rl "$baseNomeCamel" ./$newComponent | xargs sed -i "s/$baseNomeCamel/$newNomeCamel/g"
echo "$baseNomePascal -> $newNomePascal"
grep -rl "$baseNomePascal" ./$newComponent | xargs sed -i "s/$baseNomePascal/$newNomePascal/g"
echo "$baseNomeConst -> $newNomeConst"
grep -rl "$baseNomeConst" ./$newComponent | xargs sed -i "s/$baseNomeConst/$newNomeConst/g"
echo "$baseNomeDot -> $newNomeDot"
grep -rl "$baseNomeDot" ./$newComponent | xargs sed -i "s/$baseNomeDot/$newNomeDot/g"
echo "$baseNomeCapital -> $newNomeCapital"
grep -rl "$baseNomeCapital" ./$newComponent | xargs sed -i "s/$baseNomeCapital/$newNomeCapital/g"
echo "$baseNomeCram -> $newNomeCram"
grep -rl "$baseNomeCram" ./$newComponent | xargs sed -i "s/$baseNomeCram/$newNomeCram/g"
